import HomeIcon from '@mui/icons-material/Home';
import LiveTvIcon from '@mui/icons-material/LiveTv';
import TopicIcon from '@mui/icons-material/Topic';
import { AppBar, Box, Divider, Toolbar } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import { isEmpty, uniqueId } from 'lodash';
import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Input from 'src/helpers/Input';
import Text from 'src/helpers/Text';
import { COLORS } from 'src/utils/colors';
import { logo } from 'src/utils/imageAssets';
import styles from './styles';
import { fetchListKeyWord } from '../../redux/actions/search';

interface HeaderComponentProps {
    open?: boolean,
    search?: string,
    handleSearch?: (e: any)=> void,
    handleCloseDrawer?: any,
    scrollPage?: boolean,
    handleOpenMenuMobile: (status: boolean) => void,
    activeMenu: boolean,
    handleChangeActiveMenu: (id: number)=> void,
    menuActive: number,
}

const HeaderComponent = (props: HeaderComponentProps & WithStyles<typeof styles>) => {
    const {
        classes,
        handleChangeActiveMenu,
        menuActive
    } = props;

    const [search, setSearch] = React.useState('');

    const menuList: {
        id: number,
        icon: any,
        title: string,
        url: string,
    }[] = [
        {
            id: 1,
            icon: <HomeIcon/>,
            title: 'Home',
            url: '/',
        },
        {
            id: 2,
            icon: <LiveTvIcon/>,
            title: 'Now Playing',
            url: '/now-playing',
        },
        {
            id: 3,
            icon: <TopicIcon/>,
            title: 'Top Rated',
            url: '/top-rated',
        },
    ];
    const dispatch = useDispatch();
    React.useEffect(()=> {
        if(search){
            setTimeout(()=> {
                const params = {
                    query: search,
                };
                dispatch(fetchListKeyWord(params));
            }, 300 );
        }
    }, [search]);

    const handleSearchKeyWord = (e: any) => {
        setSearch(e.target.value?.trim());
    };

    const listSearchKeyword = useSelector((state: any) => state.search.listSearchKeyword);
    React.useEffect(() => {
        if(!isEmpty(listSearchKeyword)) {
            setSearch('');
        }
    }, [listSearchKeyword]);
    
  
    return (
        <React.Fragment>
            <AppBar
                color={'primary'}
                className={clsx(classes.appBarStyle)}
            >
                <div className="container">
                    <Toolbar
                        className={clsx(classes.toolbarStyle, 'w-full')}
                        disableGutters
                    >
                        <Box display={'flex'}
                            className='w-full'
                            justifyContent='space-between'
                            alignItems={'center'}>
                            <Box component={'a'}
                                href={'/'}>
                                <img src={logo}
                                    className={classes.logo}
                                    alt="" />
                            </Box>
                            
                            <Box className="box-search">
                                <Input onChange={handleSearchKeyWord}
                                    placeholder='Search for a movie, tv show,...'/>
                                <Box className="content-search">
                                    <Box className="mb-3"><Text baseSB1
                                        color={COLORS.black}>Movie Search:</Text>
                                    </Box>
                                    <Divider/>
                                    {!isEmpty(listSearchKeyword) ? listSearchKeyword.slice(0, 4).map(item => (
                                        <Box component={'a'}
                                            display="flex"
                                            key={uniqueId()}
                                            alignItems={'center'}
                                            href={`/movie/${item.id}-${item.name}`}>
                                            <Text base1
                                                color={COLORS.grey5}>
                                                {item?.name || ''}
                                            </Text>
                                        </Box>
                                    )):
                                        <Box className="not-found">
                                            <Text color={COLORS.black}>Not Found</Text>
                                        </Box>
                                    }
                                </Box>
                            </Box>
                            <Box component={'ul'}
                                marginX={4}
                                className={classes.listMenu}>
                                {!isEmpty(menuList) && menuList.map(li => (
                                    <Box display={'flex'}
                                        key={uniqueId()}
                                        marginRight={3}
                                        className={clsx({'active': li.id === menuActive})}
                                        onClick={()=> handleChangeActiveMenu(li.id)}
                                        component={'li'}>
                                        <Box component='a'
                                            display={'flex'}
                                            alignItems='center'
                                            href={li.url}>
                                            <Text bodySB1> 
                                                {li.icon} 
                                                {li.title}</Text>
                                        </Box>
                                    </Box>
                                ))}
                            </Box>
                        </Box>
                        
                    </Toolbar>
                </div>
            </AppBar>
        </React.Fragment>
    );
};
export default withStyles(styles)(HeaderComponent);