import _ from 'lodash';
import * as React from 'react';
import {Outlet} from 'react-router-dom';
import HeaderComponent from './HeaderComponent';

const Layout = () => {
    const [menuActive, setMenuActive] = React.useState(1);
    const [search, setSearch] = React.useState<string>('');
    const [activeMenu, setActiveMenu] = React.useState<boolean>(false);
    
    React.useEffect(()=> {
        if(window.location.pathname === '/') {
            setMenuActive(1);
        } else if(window.location.pathname === '/now-playing') {
            setMenuActive(2);
        } else if(window.location.pathname === '/top-rated') {
            setMenuActive(3);
        }
    },[]);
    
    const handleSearch = (e: any)=> {
        if(!_.isEmpty(e))
            setSearch(e.target.value);
        else  setSearch('');
    };

    const handleOpenMenuMobile = (status: boolean) => {
        setActiveMenu(status);
    };

    const handleChangeActiveMenu = (id: number) => {
        setMenuActive(id);
    };
    return (
        <>
            <HeaderComponent search={search}
                handleOpenMenuMobile={handleOpenMenuMobile}
                activeMenu={activeMenu}
                handleChangeActiveMenu={handleChangeActiveMenu}
                menuActive={menuActive}
                handleSearch={handleSearch}/>
            <div className="container content-page">
                <Outlet/>
            </div>
        </>
    );
};

export default Layout;