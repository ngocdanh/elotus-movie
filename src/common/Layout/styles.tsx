import {createStyles} from '@mui/styles';
import { Theme } from '@mui/material';
import { COLORS } from 'src/utils/colors';

const styles = (theme: Theme) => createStyles({
    root: {
        
    },
    styleLogo: {
        width: 48,
        height: 48
    },
    slidebar: {
        padding: 24,
        '& .logo': {
            [theme.breakpoints.down('sm')]: {
                display: 'block',
                position: 'absolute',
                top: 20,
                right: 20,
            }
        }
    },
    appBarStyle: {
        padding: 20,
        boxShadow: 'inset 2px -1px 3px 0px #f4f4f4 !important',
        transition: 'all .3s ease',
        '&.active': {
            zIndex: '4000 !important',
            borderBottom: `1px solid ${COLORS.grey3}`
        },
        '@media(max-width: 1280px)': {
        },
        [theme.breakpoints.down('sm')]: {
            paddingLeft: 0,
            padding: 10,
        }
    },
    
    toolbarStyle: {
        display: 'flex',
        justifyContent: 'space-between',
        '& .group-search': {
            width: 360,
            position: 'relative',
        },
        '& .open-menu': {
            display: 'none',
            [theme.breakpoints.down('sm')]: {
                display: 'block',
            }
        },
        '& .box-search': {
            position: 'relative',
            '& .not-found': {
                minHeight: 100,
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
            },
            '& .content-search': {
                position: 'absolute',
                top: 'calc(100% + 5px)',
                zIndex: 2000, 
                left: 0,
                right: 0,
                minHeight: 100,
                background: COLORS.white,
                borderRadius: '12px',
                boxShadow: 'inset 2px -1px 3px 0px #f4f4f4 !important',
                padding: 30,
                transition: 'all .3s ease',
                visibility:'hidden',
                opacity: 0,
                '& img': {
                    width: 40,
                    marginRight: 15,
                },
                '& a .text': {
                    marginTop: 20,
                    '&:hover': {
                        color: `${COLORS.green} !important`,
                    }
                },
                
            },
            '&:hover .content-search': {
                visibility:'visible',
                opacity: 1,
            }
        }
    }, 
    logo: {
        width: 147,
        height: 'auto',
        objectFit: 'contain',
    },
    listMenu: {
        display: 'flex',
        '& li': {
            cursor: 'pointer',
            '& .text:hover': {
                color: `${COLORS.green} !important`
            },
            '& svg': {
                marginRight: 8,
            },
            '&.active .text': {
                color: `${COLORS.green} !important`
            }
        }
    }
});

export default styles;