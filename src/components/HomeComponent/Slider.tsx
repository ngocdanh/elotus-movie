import { Box, Skeleton } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import styles from './styles';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import { Autoplay, Navigation } from 'swiper';
import { ISliderComponent } from './types';
import { get, isEmpty, round, uniqueId } from 'lodash';
import { API_IMAGE_ORIGINAL } from '../../common/config';
import Text from 'src/helpers/Text';
import { COLORS } from 'src/utils/colors';
import StarIcon from '@mui/icons-material/Star';
import { convertDataMovieToURL } from 'src/utils';

function SliderComponent(props: ISliderComponent & WithStyles<typeof styles>) {
    const {
        classes,
        listMovieGenre,
        listMovieTrending,
        limit,
    } = props;
    const navigate = useNavigate();

    return (
        <Box className={classes.topSlider}>
            <Swiper
                spaceBetween={50}
                slidesPerView={2}
                navigation={true} 
                modules={[Navigation, Autoplay]} 
                autoplay={{
                    delay: 3500,
                    disableOnInteraction: false,
                }}
            >

               
                {!isEmpty(listMovieTrending) ? listMovieTrending.slice(0, limit).map(movie => (
                    <SwiperSlide key={movie.id}>
                        <Box className={'movie-item'}>
                            <img src={`${API_IMAGE_ORIGINAL}${movie.backdrop_path}`}
                                alt={movie.title} />
                            <Box className="content">
                                <a href={convertDataMovieToURL(movie, 'movie')}>
                                    <Text h3
                                        className='movie-item__title'
                                        color={COLORS.white}>{movie.title}</Text>
                                </a>
                                <Box component='ul'
                                    className="group-info">
                                    <Box display={'flex'}
                                        component='li'>
                                        <StarIcon/>
                                        <Text captionM2
                                            color={COLORS.white}>
                                            {round(movie.vote_average, 2)}
                                        </Text>
                                    </Box>
                                    <Box display={'flex'}
                                        component='li'>
                                        <Text captionM2
                                            color={COLORS.white}>
                                            {movie.release_date}
                                        </Text>
                                    </Box>
                                    {!isEmpty(movie.genre_ids) && movie.genre_ids.map(genre => {
                                        const genreName = !isEmpty(listMovieGenre) 
                                            ? listMovieGenre.find(o => o.id === genre) :'';
                                        return (
                                            <Box display={'flex'}
                                                key={uniqueId()}
                                                component='li'>
                                                <Text captionM1
                                                    color={COLORS.white}>
                                                    {get(genreName, 'name', '')}
                                                </Text>
                                            </Box>
                                        );
                                    })}
                                        
                                </Box>
                            </Box>
                        </Box>
                    </SwiperSlide>
                )):
                    <div className="flex">
                        <Skeleton variant="rectangular"
                            style={{borderRadius: '12px'}}
                            width={'calc(50% - 25px)'}
                            animation="wave"
                            height={450} />
                        <Skeleton variant="rectangular"
                            style={{marginLeft: 50, borderRadius: '12px'}}
                            width={'calc(50% - 25px)'}
                            animation="wave"
                            height={450} 
                        />
                    </div>
                }
            </Swiper>
        </Box>
    );
}

export default withStyles(styles)(SliderComponent);