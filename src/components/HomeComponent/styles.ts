import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import { COLORS } from 'src/utils/colors';

const styles = (theme: any) => createStyles({
    root: {
        
    },
    topSlider: {
        '& .movie-item': {
            position: 'relative',
            borderRadius: '12px',
            overflow: 'hidden',
            '& img': {
                width: '100%',
                height: 'auto',
                objectFit: 'contain',
            },
            '&__title': {
                textShadow: '-1px 0px 2px rgba(0,0,0,0.6)',
                '&:hover': {
                    color: `${COLORS.green} !important`
                }
            },
            '& .content': {
                position: 'absolute',
                bottom: 50,
                left: 0,
                right: 0,
                maxWidth: 500,
                padding: 30,
                '& ul.group-info': {
                    display: 'flex',
                    '& li': {
                        marginRight: 10,
                        color: COLORS.white,
                        '& svg': {
                            marginRight: 4,
                        }
                    }
                }
            }
        }
    },
    movieUpcoming: {
        marginTop: 50,
        marginBottom: 50,
        '& .movie-coming-item': {
            
        }
    }
});

export default styles;