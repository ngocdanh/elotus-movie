export interface ISliderComponent {
    listMovieGenre: {
        id: number,
        name: string,
    }[],
    listMovieTrending: {
        backdrop_path: string,
        poster_path: string,
        genre_ids: [],
        id: number,
        original_title: string,
        title: string,
        vote_average: number,
        vote_count: number,
        release_date: string,
    }[],
    limit: number,
}
export interface IHomeComponent {
    listMovieGenre: {
        id: number,
        name: string,
    }[],
    listMovieTrending: {
        backdrop_path: string,
        poster_path: string,
        genre_ids: [],
        id: number,
        original_title: string,
        title: string,
        vote_average: number,
        vote_count: number,
        release_date: string,
    }[],
    listMovieLaster: {
        backdrop_path: string,
        poster_path: string,
        genre_ids: [],
        id: number,
        original_title: string,
        title: string,
        vote_average: number,
        vote_count: number,
        release_date: string,
        overview: string,
    }[]
}
export interface IUpcomingComponent {
    listMovieTrending: {
        backdrop_path: string,
        poster_path: string,
        genre_ids: [],
        id: number,
        original_title: string,
        title: string,
        vote_average: number,
        vote_count: number,
        release_date: string,
    }[],
    limit: number,
}
export interface IMovieLasterComponent {
    listMovieLaster: {
        backdrop_path: string,
        poster_path: string,
        genre_ids: [],
        id: number,
        original_title: string,
        title: string,
        vote_average: number,
        vote_count: number,
        release_date: string,
    }[],
    limit: number,
}