import { Box, Grid, Skeleton } from '@mui/material';
import { WithStyles, withStyles } from '@mui/styles';
import { isEmpty, uniqueId } from 'lodash';
import React from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { API_IMAGE_LARGE } from 'src/common/config';
import MovieCard from 'src/helpers/MovieCard';
import Text from 'src/helpers/Text';
import { COLORS } from 'src/utils/colors';
import 'swiper/css';
import 'swiper/css/navigation';
import styles from './styles';
import { INowPlayingComponent } from './types';
import ListIcon from '@mui/icons-material/List';
import { CARD, LIST } from 'src/utils/constants';
import ViewCard from './viewCard';
import ViewModuleIcon from '@mui/icons-material/ViewModule';
import clsx from 'clsx';
import ViewList from './viewList';

function NowPlayingComponent(props: INowPlayingComponent & WithStyles<typeof styles>) {
    const {
        classes,
        listNowPlaying,
        handleChangeView,
        view,
    } = props;
    return (
        <Box className={classes.root}>
            <Box className={classes.movieUpcoming}>
                <div className="flex items-center justify-between my-4">
                    <Box className="title-element">
                        <Text h4
                            color={COLORS.black}>Now Playing</Text>
                    </Box>
                    <Box className="change-view">
                        <Box>
                            <ViewModuleIcon className={clsx({'active': view === CARD})}
                                onClick={()=> handleChangeView(CARD)}/>
                            <ListIcon className={clsx({'active': view === LIST})}
                                onClick={()=> handleChangeView(LIST)}/>
                        </Box>
                    </Box>
                </div>
                {view === CARD && (
                    <ViewCard
                        listNowPlaying={listNowPlaying}
                    />
                )} 
                {view === LIST && (
                    <ViewList
                        listNowPlaying={listNowPlaying}
                    />
                )} 
            </Box>
        </Box>
    );
}

export default withStyles(styles)(NowPlayingComponent);