import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import { COLORS } from 'src/utils/colors';

const styles = (theme: any) => createStyles({
    root: {
        
    },
    topSlider: {
        '& .movie-item': {
            position: 'relative',
            borderRadius: '12px',
            overflow: 'hidden',
            '& img': {
                width: '100%',
                height: 'auto',
                objectFit: 'contain',
            },
            '&__title': {
                textShadow: '-1px 0px 2px rgba(0,0,0,0.6)',
                '&:hover': {
                    color: `${COLORS.green} !important`
                }
            },
            '& .content': {
                position: 'absolute',
                bottom: 50,
                left: 0,
                right: 0,
                maxWidth: 500,
                padding: 30,
                '& ul.group-info': {
                    display: 'flex',
                    '& li': {
                        marginRight: 10,
                        color: COLORS.white,
                        '& svg': {
                            marginRight: 4,
                        }
                    }
                }
            }
        }
    },
    movieUpcoming: {
        marginTop: 50,
        marginBottom: 50,
        '& .movie-coming-item': {
            
        },
        '& .change-view': {
            padding: 10,
            backgroundColor: COLORS.greenLight,
            borderRadius: '4px',
            maxWidth: 'max-content',
            marginTop: 10,
            marginBottom: 10,
            display: 'flex',
            '& svg': {
                width: 24,
                height: 24,
                color: COLORS.black,
                marginRight: 20,
                '&.active': {
                    color: COLORS.green,
                }
            }
        },
        '& .list-movie': {
            '& img': {
                width: 200,
                borderRadius: '12px',
            },
            '& .overview': {
                maxWidth: 600,
            },
            '& .item': {
              
            },
            '& .title': {
                '&:hover': {
                    color: `${COLORS.green} !important`
                }
            }
        }
    }
});

export default styles;