import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import TopRatedComponent from 'src/components/TopRatedComponent';
import * as movieActions from 'src/redux/actions/movies';
import { CARD } from 'src/utils/constants';

const TopReadContainer = () => {
    const [view, setView] = useState(CARD);
    const dispatch = useDispatch();
    const listMovieTopRated = useSelector((state: any) => state.movie.listMovieTopRated);
    
    useEffect(()=> {
        dispatch(movieActions.fetchListTopRated());
    }, []);

    const handleChangeView = (value: string)=> {
        setView(value);
    };

    return (
        <TopRatedComponent
            listMovieTopRated={listMovieTopRated}
            view={view}
            handleChangeView={handleChangeView}
        />
    );
};

export default TopReadContainer; 