import { Backdrop, Box, Fade, Modal, SxProps } from '@mui/material';
import { withStyles, WithStyles } from '@mui/styles';
import clsx from 'clsx';
import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { openModal } from '../../redux/actions/global';
import styles from './styles';
import CloseIcon from '@mui/icons-material/Close';

interface IModalCustom {
    children: any,
    title: string,
    width?: number,
    height?: number,
    style?: any,
    sx?: SxProps,
    className?: string,
}


const ModalCustom = (props: IModalCustom & WithStyles<typeof styles>) => {
    const {
        classes, 
        className,
        children, 
        title,
        width,
        height,
        style, 
        sx,
        ...otherProps} = props;

    const modalName = useSelector((state: any) => state.global.modal);
    
    const dispatch = useDispatch();
    const onClose = (e: any) => {
        dispatch(openModal('', null));
    };
    const open = modalName.title ===title;
    return (
        <Modal
            open={open}
            onClose={onClose}
            aria-labelledby={'modal-custom'}
            aria-describedby={title}
            BackdropProps= {{
                timeout: 0,
                classes: {
                    root: classes.removeBorder,
                }
            }}
            classes={{
                root: classes.removeBorder
            }}
            BackdropComponent={Backdrop}
        >
            <Fade in={open}>
                <Box component={'div'}
                    className={clsx(classes.modalBox, 'modal-custom')}
                    style={{
                        ...width && {width},
                        ...height && {minHeight: height},
                    }}
                >
                    <Box className="content">
                        <Box className={'box-title'}
                            display='flex'
                            justifyContent={'flex-end'}>
                            <Box className="cursor-pointer"
                                onClick={onClose}>
                                <CloseIcon/>
                            </Box>
                        </Box>
                        {children}
                    </Box>
                </Box>
            </Fade>

        </Modal>
    );
};

export default withStyles(styles)(ModalCustom);