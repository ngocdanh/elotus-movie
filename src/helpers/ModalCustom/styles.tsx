import { COLORS } from 'src/utils/colors';
import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';

const styles = (theme: any) => createStyles({
    modalBox: {
        borderRadius: '12px',
        boxShadow: '0px 10px 15px -3px rgba(0,0,0,0.1)',
        background: COLORS.white,
        position: 'absolute' as const,
        top: '50%',
        left: '50%',
        overflow: 'hidden',
        transform: 'translate(-50%, -50%)',
        '& .content': {
            position: 'relative',
            padding: 20,
            paddingTop: 80,

        },
        '& img.bg-modal': {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            filter: 'brightness(.6)',
            zIndex: -1,
        },

        '& .box-title': {
            display: 'flex',
            justifyContent: 'space-between',
            marginBottom: 20,
            position: 'fixed',
            top: 30,
            right: 30,
            height: 36,
            zIndex: 2,
            '& .title': {
        
            },
            '& svg': {
                color: COLORS.white,
                width: 30,
                height: 30,
            },
            '& .box-close': {

            },
            '& .icon-btn-closed': {
                width: 36,
                height: 36,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: COLORS.grey3,
                borderRadius: '50%',
                transition: 'all .3s ease',
                boxShadow: '0px 2px 8px rgba(0, 0, 0, 0.06)',
                '& .icon-svg': {
                    width: 20,
                    height: 20,
                    transition: 'all .3s ease',
                },
                '&:hover': {
                    '& .icon-svg': {
                        transform: 'rotate(90deg)',
                    }
                },
                '&.white': {
                    backgroundColor: COLORS.white,
                }
            }
        },
    },
    fullPage: {
        width: '100%',
        height: '100%',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        borderRadius: 0,
        transform: 'unset',
        backgroundColor: COLORS.grey3,
        backdropFilter: 'unset !important',
        '&:focus-visible': {
            outline: 'unset !important'
        }
    },
    removeBorder: {
        '&:focus-visible': {
            outline: 'none',
        }
    }
});

export default styles;