import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import { COLORS } from 'src/utils/colors';

const styles = (theme: any) => createStyles({
    root: {
        '& .movie-item': {
            position: 'relative',
            overflow: 'hidden',
            borderRadius: '12px',
            '& img': {
                width: '100%',
                overflow: 'hidden',
                borderRadius: '12px',
                transition: 'all .2s ease',
            },
            '& .content': {
                marginTop: 15,
                marginBottom: 15,
                '& .text': {
                    color: COLORS.white,
                },
                '& .title': {
                    '&:hover': {
                        color: `${COLORS.green} !important`,
                    }
                }
            },
            '& .group-action': {
                display: 'flex',
                position: 'absolute',
                bottom: '40%',
                left: 'calc(50% - 20px)',
                borderRadius: '4px',
                justifyContent: 'center',
                padding: '2px 4px',
                background: '#ffffff4a',
                transition: 'all .2s ease',
                opacity: 0,
                visibility: 'hidden',
                '& svg': {
                    width: 25,
                    color: COLORS.green,
                },
                '& svg:hover': {
                    color: COLORS.orangeLight,
                },
            },
            '&:hover': {
                '& img': {
                    opacity: 0.9
                },
                '& .group-action': {
                    transform: 'translateY(-15px)',
                    opacity: 1,
                    visibility: 'visible',
                   
                }
            }
        }
    },
});

export default styles;