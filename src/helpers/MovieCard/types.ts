export interface IMovieCardComponent {
    movie: {
        backdrop_path: string,
        poster_path: string,
        genre_ids: [],
        id: number,
        original_title: string,
        title?: string,
        vote_average: number,
        vote_count: number,
        release_date: string,
        name?: string,
    },
    genre: string
}