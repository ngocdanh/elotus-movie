import {createStyles} from '@mui/styles';
import {} from '@mui/styles/';
import borderMenu from '@assets/images/borderMenu.png';
import { COLORS } from 'src/utils/colors';

const styles = (theme: any) => createStyles({
    arrow: {
        '&:before': {
            color: COLORS.green,
        }
    },
    tooltip: {
        backgroundColor: COLORS.green,
    }
});

export default styles;