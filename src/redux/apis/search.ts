import queryString from 'query-string';
import { API_SEARCH_KEYWORD, API_TV_MOVIE_LASTER } from 'src/common/apiEndPoits';
import axiosServices from 'src/common/axiosServices';
import { GET } from 'src/utils/constants';

export const fetchListSearchKeyWordAPI = (params = {}) => {

    
    const uri = `${API_SEARCH_KEYWORD}`;
    return axiosServices({
        method: GET,
        url: uri,
        params,
    });
};

