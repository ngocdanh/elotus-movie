import queryString from 'query-string';
import { API_TV_MOVIE_LASTER } from 'src/common/apiEndPoits';
import axiosServices from 'src/common/axiosServices';
import { GET } from 'src/utils/constants';

export const fetchListTVShowAPI = (params = {}) => {
    let queryParams = '';
    if (Object.keys(params).length > 0) {
        try {
            queryParams = `?${queryString.stringify(params)}`;
        } catch (err) {
            queryParams = '';
        }
    }
    
    const uri = `${API_TV_MOVIE_LASTER}${queryParams}`;
    return axiosServices({
        method: GET,
        url: uri,
    });
};

