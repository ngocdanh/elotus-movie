import * as movieConstants from '../constants/movies';
import { FETCH_LIST_MOVIE_DETAIL } from '../constants/movies';

const initialState = {
    isLoading: true,
    error: null,
    listMovieGenre: null,
    listMovieTrending: null,
    movieDetail: null,
    listMovieNowPlaying: null,
    listMovieTopRated: null,
};

const reducer = (state = initialState, action: { type: any; payload: { data?: any; error?: any; }; }) => {
    switch(action.type) {
        /* Fetch list movie genre */
        case movieConstants.FETCH_LIST_MOVIE_GENRE: {
            return {
                ...state,
                isLoading: true,
                error: null,
                listMovieGenre: null
            };
        }
        case movieConstants.FETCH_LIST_MOVIE_GENRE_SUCCESS: {
            const { data } = action.payload;
            
            return {
                ...state,
                isLoading: false,
                error: null,
                listMovieGenre: data.genres,
            };
        }
        case movieConstants.FETCH_LIST_MOVIE_GENRE_FAILED: {
            const { error } = action.payload;
            return {
                ...state,
                isLoading: false,
                error: error,
                listMovieGenre: null,
            };
        }
        /* Fetch list movie trending of week */
        case movieConstants.FETCH_LIST_MOVIE_TRENDING_WEEK: {
            return {
                ...state,
                isLoading: true,
                error: null,
                listMovieTrending: null
            };
        }
        case movieConstants.FETCH_LIST_MOVIE_TRENDING_WEEK_SUCCESS: {
            const { data } = action.payload;
            return {
                ...state,
                isLoading: false,
                error: null,
                listMovieTrending: data.results,
            };
        }
        case movieConstants.FETCH_LIST_MOVIE_TRENDING_WEEK_FAILED: {
            const { error } = action.payload;
            return {
                ...state,
                isLoading: false,
                error: error,
                listMovieTrending: null,
            };
        }
        case movieConstants.FETCH_LIST_MOVIE_DETAIL: {
            return {
                ...state,
                isLoading: true,
                error: null,
                listMovieTrending: null
            };
        }
        case movieConstants.FETCH_LIST_MOVIE_DETAIL_SUCCESS: {
            const { data } = action.payload;
            return {
                ...state,
                isLoading: false,
                error: null,
                movieDetail: data,
            };
        }
        case movieConstants.FETCH_LIST_MOVIE_DETAIL_FAILED: {
            const { error } = action.payload;
            
            return {
                ...state,
                isLoading: false,
                error: error,
                movieDetail: null,
            };
        }
        case movieConstants.FETCH_LIST_NOW_PLAYING: {
            return {
                ...state,
                isLoading: true,
                error: null,
                listMovieNowPlaying: null
            };
        }
        case movieConstants.FETCH_LIST_NOW_PLAYING_SUCCESS: {
            const { data } = action.payload;
            return {
                ...state,
                isLoading: false,
                error: null,
                listMovieNowPlaying: data.results,
            };
        }
        case movieConstants.FETCH_LIST_NOW_PLAYING_FAILED: {
            const { error } = action.payload;
            return {
                ...state,
                isLoading: false,
                error: error,
                listMovieNowPlaying: null,
            };
        }
        case movieConstants.FETCH_LIST_TOP_RATED: {
            return {
                ...state,
                isLoading: true,
                error: null,
                listMovieTopRated: null
            };
        }
        case movieConstants.FETCH_LIST_TOP_RATED_SUCCESS: {
            const { data } = action.payload;
            return {
                ...state,
                isLoading: false,
                error: null,
                listMovieTopRated: data.results,
            };
        }
        case movieConstants.FETCH_LIST_TOP_RATED_FAILED: {
            const { error } = action.payload;
            return {
                ...state,
                isLoading: false,
                error: error,
                listMovieTopRated: null,
            };
        }
        default: {
            return {
                ...state,
            };
        }
    }
};

export default reducer;