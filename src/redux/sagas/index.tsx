import { fork } from 'redux-saga/effects';
import { WatchGlobal } from './global';
import { WatchMovies } from './movies';
import { WatchSearch } from './search';
import { WatchTVShow } from './tvShow';


function *rootSaga() {
    yield fork(WatchMovies);
    yield fork(WatchTVShow);
    yield fork(WatchGlobal);
    yield fork(WatchSearch);
}

export default rootSaga;