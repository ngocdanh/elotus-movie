import { STATUS_CODES } from 'src/utils/constants'; 
import {
    call,
    delay, put,
    takeLatest,
} from 'redux-saga/effects';
import { hideLoading, showLoading } from '../actions/global';
import { fetchListMovieDetailAPI, fetchListMovieGenreAPI, fetchListMovieNowPlayingAPI, fetchListMovieTopRatedAPI, fetchListMovieTrendingWeekAPI } from '../apis/movies';
import { FETCH_LIST_MOVIE_GENRE, FETCH_LIST_MOVIE_TRENDING_WEEK, FETCH_LIST_MOVIE_DETAIL, FETCH_LIST_TOP_RATED, FETCH_LIST_NOW_PLAYING } from '../constants/movies';
import { 
    fetchListMovieGenreFailed,
    fetchListMovieGenreSuccess, 
    fetchListMovieTrendingWeekFailed,
    fetchListMovieTrendingWeekSuccess,
    fetchListNowPlayingFailed,
    fetchListNowPlayingSuccess,
    fetchListTopRatedFailed,
    fetchListTopRatedSuccess,
    fetchMovieDetailFailed,
    fetchMovieDetailSuccess
} from '../actions/movies';

interface actionType {
    type: string;
    payload?: any;
    meta?: any;
    error?: boolean;
}

function* actionFetchListMovieGenreType(action: actionType): any {
    try {
	    yield put(showLoading());
	    yield delay(200);
        const { params } = action.payload;
        const response = yield call(fetchListMovieGenreAPI, params); 
        
        const { status, data } = response;

        if (status === STATUS_CODES.success) {
		    yield put(fetchListMovieGenreSuccess(data));
	    } else {
        	yield put(fetchListMovieGenreFailed(response));
        }
	    yield put(hideLoading());
    } catch (error) {
        yield put(fetchListMovieGenreFailed(error));
    }
}

function* actionFetchListMovieTrendingWeekType(action: actionType): any {
    try {
        yield put(showLoading());
        yield delay(500);
        const { params } = action.payload;
        const response = yield call(fetchListMovieTrendingWeekAPI, params); 
      
        const { status, data } = response;

        if (status === STATUS_CODES.success) {
            yield put(fetchListMovieTrendingWeekSuccess(data));
        } else {
            yield put(fetchListMovieTrendingWeekFailed(response));
        }
        yield put(hideLoading());
    } catch (error) {
        yield put(fetchListMovieTrendingWeekFailed(error));
    }
}
function* actionFetchListMovieDetailType(action: actionType): any {
    try {
        yield put(showLoading());
        yield delay(500);
        const { params } = action.payload;
        const response = yield call(fetchListMovieDetailAPI, params); 
      
        const { status, data } = response;
        

        if (status === STATUS_CODES.success) {
            yield put(fetchMovieDetailSuccess(data));
        } else {
            yield put(fetchMovieDetailFailed(response));
        }
        yield put(hideLoading());
    } catch (error) {
        yield put(fetchListMovieTrendingWeekFailed(error));
    }
}
function* actionFetchListTopRatedType(action: actionType): any {
    try {
        yield put(showLoading());
        yield delay(500);
        const { params } = action.payload;
        const response = yield call(fetchListMovieTopRatedAPI, params); 
      
        const { status, data } = response;

        if (status === STATUS_CODES.success) {
            yield put(fetchListTopRatedSuccess(data));
        } else {
            yield put(fetchListTopRatedFailed(response));
        }
        yield put(hideLoading());
    } catch (error) {
        yield put(fetchListTopRatedFailed(error));
    }
}
function* acionFetchListNowPlayingType(action: actionType): any {
    try {
        yield put(showLoading());
        yield delay(500);
        const { params } = action.payload;
        const response = yield call(fetchListMovieNowPlayingAPI); 
      
        const { status, data } = response;

        if (status === STATUS_CODES.success) {
            yield put(fetchListNowPlayingSuccess(data));
        } else {
            yield put(fetchListNowPlayingFailed(response));
        }
        yield put(hideLoading());
    } catch (error) {
        yield put(fetchListNowPlayingFailed(error));
    }
}

export function *WatchMovies() {
    yield takeLatest(FETCH_LIST_MOVIE_GENRE, actionFetchListMovieGenreType);
    yield takeLatest(FETCH_LIST_MOVIE_TRENDING_WEEK, actionFetchListMovieTrendingWeekType);
    yield takeLatest(FETCH_LIST_MOVIE_DETAIL, actionFetchListMovieDetailType);
    yield takeLatest(FETCH_LIST_TOP_RATED, actionFetchListTopRatedType);
    yield takeLatest(FETCH_LIST_NOW_PLAYING, acionFetchListNowPlayingType);
}