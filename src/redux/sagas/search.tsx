import {
    call,
    delay, put,
    takeLatest
} from 'redux-saga/effects';
import { STATUS_CODES } from 'src/utils/constants';
import { hideLoading, showLoading } from '../actions/global';
import { fetchListKeyWordFailed, fetchListKeyWordSuccess } from '../actions/search';
import { fetchListTVShowFailed, fetchListTVShowSuccess } from '../actions/tvShow';
import { fetchListSearchKeyWordAPI } from '../apis/search';
import { fetchListTVShowAPI } from '../apis/tvShow';
import { FETCH_LIST_SEARCH_KEYWORD } from '../constants/search';
import { FETCH_LIST_TV_POPULAR } from '../constants/tvShow';

interface actionType {
    type: string;
    payload?: any;
    meta?: any;
    error?: boolean;
}

function* actionFetchListSearchKeywordType(action: actionType): any {
    try {
	    yield put(showLoading());
	    yield delay(200);
        const { params } = action.payload;
        const response = yield call(fetchListSearchKeyWordAPI, params); 
        
        const { status, data } = response;

        if (status === STATUS_CODES.success) {
		    yield put(fetchListKeyWordSuccess(data));
	    } else {
        	yield put(fetchListKeyWordFailed(response));
        }
	    yield put(hideLoading());
    } catch (error) {
        yield put(fetchListKeyWordFailed(error));
    }
}

export function *WatchSearch() {
    yield takeLatest(FETCH_LIST_SEARCH_KEYWORD, actionFetchListSearchKeywordType);
}