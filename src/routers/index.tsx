import React from 'react';
import { Navigate, useRoutes } from 'react-router-dom';
import Layout from 'src/common/Layout';
import HomeContainer from 'src/containers/HomeContainer';
import MovieDetailContainer from 'src/containers/MovieDetailContainer';
import NowPlayingContainer from 'src/containers/NowPlayingContainer';
import TopReadContainer from 'src/containers/TopRatedContainer';

export default () => {
    const router = [
        {
            path: '/',
            element: <Layout/>,
            children: [
                {path: '/', element: <HomeContainer/>,},
                {path: '/movie/:id', element: <MovieDetailContainer/>},
                {path: '/now-playing', element: <NowPlayingContainer/>},
                {path: '/top-rated', element: <TopReadContainer/>}
            ]
        },
        {
            path: '*',
            element: <Navigate to="/"
                replace />
        }
    ];
    return useRoutes(router);
};